﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Threading.Tasks;

namespace oli
{
    public class BlobUploader
    {
        private readonly CloudBlobClient cloudBlobClient;

        public BlobUploader(string connectionString)
        {
            var storageAccount = CloudStorageAccount.Parse(connectionString);
            this.cloudBlobClient = storageAccount.CreateCloudBlobClient();
        }

        public async void UploadBlobAsync(string containerName, string blobName, string content)
        {
            if (string.IsNullOrWhiteSpace(containerName) || string.IsNullOrWhiteSpace(blobName) || string.IsNullOrWhiteSpace(content))
            {
                return; // fast return - could add fail message 
            }

            // Blobs and containers have to be named with lower case
            var lowerContainerName = containerName.ToLower();
            var lowerblobName = blobName.ToLower();

            try
            {
                var container = await this.CreateContainerAsync(lowerContainerName).ConfigureAwait(false);
                var blob = container.GetBlockBlobReference(lowerblobName);

                await blob.UploadTextAsync(content).ConfigureAwait(false);
            }
            catch (Exception)
            {
                // Unhandled exception could bring down entire program
                // TODO: Handle Exception
                throw;
            }
        }

        public async void UploadBlobAsync(Subscription sub)
        {
            // Get values from subscription.
            try
            {
                this.UploadBlobAsync(sub.Container, sub.BlobName, sub.ToJSON());
            }
            catch (Exception)
            {
                // TODO: Handle Exception
                throw;
            }
        }

        private async Task<CloudBlobContainer> CreateContainerAsync(string containerName)
        {
            // Azure specific code
            var container = this.cloudBlobClient.GetContainerReference(containerName);
            if (await container.CreateIfNotExistsAsync().ConfigureAwait(false))
            {
                var permissions = new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob };
                await container.SetPermissionsAsync(permissions).ConfigureAwait(false);
            }

            return container;
        }
    }
}
