﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json.Linq;
using System;
/* About
* 
*  Oli JSON Generator 
*  Creates JSON strings to subscribe to sensors. (or something)
* 
* 
*  Written by:
*      - Alex Dawkins      <alexander.dawkins@gmail.com>
*      - Kier Wills        <kierwills@gmail.com>
*      - Jack Campaigne    <j.campaigne@fugro.com>
*  
*  
*  October 2018
* 
*      ^..^      /
*      /_/\_____/
*         /\   /\
*        /  \ /  \
*/
namespace oli.Pages
{
    public class IndexModel : PageModel
    {
        
        [BindProperty(Name = "jsonTextBox")]
        public string JSONInput { get; set; }

        public bool Valid { get; private set; } // is json valid?

        public bool Submitted { get; private set; } // stops false negatives 

        // page load
        public void OnGet()
        {
            
        }

        public void OnPost()
        {
            Submitted = true;
            if (!string.IsNullOrWhiteSpace(JSONInput)) // equivalent to Null Or Empty or Whitespace
            {
                Valid = TryParse(JSONInput);
            }
        }

        private bool TryParse(string s)
        {
            try
            {
                JToken.Parse(s);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
