using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
namespace oli.Pages
{
    public class AjaxHandler : PageModel
    {
        [BindProperty(Name = "prod")]
        public string Producer { get; set; }

        [BindProperty(Name = "sub")]
        public string Subscriber { get; set; }

        [BindProperty(Name = "param")]
        public string Parameter { get; set; }

        [BindProperty(Name = "connstr")]
        public string ConnectionString { get; set; }

        [BindProperty(Name = "active")]
        public bool Active { get; set; }

        [BindProperty(Name = "type")]
        public int Type { get; set; }

        // Make blobUploader with connection string 
        private BlobUploader blobUploader = new BlobUploader(/*Connection string to blobstore here*/);

        // On data sent
        public void OnPost()
        {
            blobUploader.UploadBlobAsync(new Subscription(Producer, Subscriber, Parameter, Active, Type, ConnectionString));
        }
    }
}