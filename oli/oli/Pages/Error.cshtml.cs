﻿using Microsoft.AspNetCore.Mvc.RazorPages;

namespace oli.Pages
{
    public class ErrorModel : PageModel
    {
        public void OnGet()
        {

        }

        public bool ShowRequestId { get; }

        public int RequestId { get; }
    }
}
