﻿document.getElementById("form").addEventListener("submit", function (event) {
    event.preventDefault();
    var AJAXParameters =
        "prod=" + $("#prod").val() +
        "&sub=" + $("#sub").val() +
        "&param=" + $("#param").val() +
        "&connstr=" + $("#connstr").val() +
        "&active=" + $('#active').is(":checked") +
        "&type=";
    ;
        
    switch ($('#type1:checked').val()) {
        case ("1"):
            AJAXParameters = AJAXParameters + "1";
            break;
        case (undefined):
            AJAXParameters = AJAXParameters + "0";
            break;
    }



    //XSRF/CSRF security feature
    //need additional header from form added by razor
    var token = document.getElementsByName("__RequestVerificationToken")[0].getAttribute("value");

    var http = new XMLHttpRequest();
    var url = '/ajaxHandler';

    http.open('POST', url, true);

    //Send the proper header information along with the request
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.setRequestHeader('RequestVerificationToken', token);

    //Call a function when the state changes.
    outputElement = "#output"
    http.onreadystatechange = function () {
        if (http.readyState == 4) {
            if (http.status == 200) {
                $(outputElement).text("Sent.");
                $(outputElement).css('color', 'green');
            } else {
                $(outputElement).text("Error, please try again.");
                $(outputElement).css('color', 'red');
            }
        }
    }

    http.send(AJAXParameters);
});
