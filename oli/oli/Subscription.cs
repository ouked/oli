﻿using Newtonsoft.Json;
using System;

namespace oli
{
    public class Subscription
    {
        // Don't put in subscription JSON
        [JsonIgnore]
        public string Container;

        [JsonIgnore]
        public string BlobName;

        [JsonProperty(PropertyName = "device")]
        public string Device { get; }

        [JsonProperty(PropertyName = "key")]
        public SubscriptionKey Key { get; }

        [JsonProperty(PropertyName = "active")]
        public bool Active { get; }

        [JsonProperty(PropertyName = "type")]
        public int Type { get; }

        [JsonProperty(PropertyName = "connectionString", NullValueHandling = NullValueHandling.Ignore)]
        public string ConnectionString { get; }

        public Subscription(string producer, string subscriber, string parameter, bool active, int type, string connectionstring = null)
        {
            this.Container = producer + "-" + parameter;
            this.BlobName = subscriber;
            this.Key = new SubscriptionKey(producer, parameter);
            this.Device = subscriber;
            this.Active = active;
            this.Type = type;

            switch (type)
            {
                case 0:
                    break;
                case 1:
                    if (string.IsNullOrWhiteSpace(connectionstring))
                    {
                        throw new Exception("Connection string is required when type is 1.");
                    }

                    this.ConnectionString = connectionstring;
                    break;

                default:
                    throw new Exception("Unrecognized Type! Use 0 or 1.");
            }
        }

        public string ToJSON()
        {
            string json = JsonConvert.SerializeObject(this);
            return json;
        }
    }
}
