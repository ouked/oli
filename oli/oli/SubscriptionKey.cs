﻿using Newtonsoft.Json;
namespace oli
{
    public class SubscriptionKey
    {
        [JsonProperty(PropertyName = "device")]
        public string Device { get; }

        [JsonProperty(PropertyName = "parameter")]
        public string Parameter { get; }

        public SubscriptionKey(string device, string parameter)
        {
            Device = device;
            Parameter = parameter;
        }
    }
}
